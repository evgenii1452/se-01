package Repository;

import Entity.Task;

import java.util.HashMap;
import java.util.Map;

public class TaskRepository {
    private Map<Integer, Task> tasks = new HashMap<>();


    public Task create(String taskName, int projectId) {
        int taskId = tasks.size() + 1;
        Task task = new Task(taskId, taskName, projectId);

        tasks.put(taskId, task);

        return task;
    }

    public Map<Integer, Task> gelAllByProjectId(int projectId) {
        Map<Integer, Task> tasks = new HashMap<>();

        for (Map.Entry<Integer, Task> task: this.tasks.entrySet()) {
            if (task.getValue().getProjectId() == projectId) {
                tasks.put(task.getKey(), task.getValue());
            }
        }

        return tasks;
    }

    public void removeById(int id) {
        tasks.remove(id);
    }

    public void removeAllByProjectId(int projectId) {
        for (Map.Entry<Integer, Task> task: tasks.entrySet()) {
            if (task.getValue().getProjectId() == projectId) {
                tasks.remove(task.getKey());
            }
        }
    }

    public Map<Integer, Task> getAll() {
        return tasks;
    }
}
