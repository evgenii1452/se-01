package Repository;

import Entity.Project;

import java.util.HashMap;
import java.util.Map;

public class ProjectRepository {
    private Map<Integer, Project> projects = new HashMap<>();

    public void removeAll() {
        projects.clear();
    }

    public Project createByName(String name) {
        int id = projects.size() + 1;
        Project project = new Project(id, name);

        projects.put(id , project);

        return project;
    }

    public Map<Integer, Project> getAll() {
        return projects;
    }

    public void removeById(int id) {
        this.projects.remove(id);
    }

    public Project getById(int id) {
        return projects.get(id);
    }
}
