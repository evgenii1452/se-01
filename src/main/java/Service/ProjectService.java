package Service;

import Entity.Project;
import Repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(
            ProjectRepository projectRepository
    ) {
        this.projectRepository = projectRepository;
    }

    public String removeAllProjects() {
        projectRepository.removeAll();

        return "[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]";
    }

    public String createProjectByName(String name) {
        Project project = projectRepository.createByName(name);

        return "[ПРОЕКТ " + project.getName() + " СОЗДАН]";
    }

    public Map<Integer, Project> getAllProjects() {
        return this.projectRepository.getAll();
    }

    public String removeProjectById(int id) {
        this.projectRepository.removeById(id);

        return "[ПРОЕКТ #" + id + " УДАЛЕН]";
    }
}
