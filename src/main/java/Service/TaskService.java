package Service;

import Entity.Project;
import Entity.Task;
import Repository.TaskRepository;

import java.util.Map;

public class TaskService {
    private TaskRepository taskRepository;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String createTask(String taskName, int projectId) {
        Task task = taskRepository.create(taskName, projectId);

        return "[ЗАДАЧА \"" + task.getName() + "\" СОЗДАНА]";
    }

    public Map<Integer, Task> getTasksByProjectId(int projectId) {
        return this.taskRepository.gelAllByProjectId(projectId);
    }

    public String removeTaskById(int id) {
        taskRepository.removeById(id);

        return "[ЗАДАЧА #" + id + " УДАЛЕНА]";
    }

    public String removeTasksByProjectId(int projectId) {
        taskRepository.removeAllByProjectId(projectId);

        return "[ЗАДАЧИ ПРОЕКТА #" + projectId + " УДАЛЕНЫ]";
    }

    public Map<Integer, Task> getAllTasks() {
        return taskRepository.getAll();
    }
}
