import Entity.Project;
import Entity.Task;
import Repository.ProjectRepository;
import Repository.TaskRepository;
import Service.ProjectService;
import Service.TaskService;

import java.util.Map;
import java.util.Scanner;

public class TaskManager {


    public static void main(String[] args) {
        ProjectService projectService = new ProjectService(new ProjectRepository());
        TaskService taskService = new TaskService(new TaskRepository());

        Scanner scanner = new Scanner(System.in);
        String command;

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Список команд:");
        showCommandList();

        do {
            command = scanner.nextLine().trim();

            switch (command) {
                case "project-clear": {
                    String response = projectService.removeAllProjects();

                    printResponse(response);
                    break;
                }
                case "project-create": {
                    System.out.println("Введите название проекта:");
                    String name = scanner.nextLine().trim();

                    String response = projectService.createProjectByName(name);

                    printResponse(response);
                    break;
                }
                case "project-list": {
                    System.out.println("[СПИСОК ПРОЕКТОВ]");
                    Map<Integer, Project> projects = projectService.getAllProjects();

                    for (Map.Entry<Integer, Project> project: projects.entrySet()){
                        System.out.println(project.getKey() + ". " + project.getValue().getName());
                    }

                    printResponse("[OK]");
                    break;
                }
                case "project-remove": {
                    System.out.println("Введите id проекта:");
                    int id = Integer.parseInt(scanner.nextLine());

                    String response = projectService.removeProjectById(id);

                    printResponse(response);
                    break;
                }
                case "task-clear": {
                    System.out.println("Введите id проекта:");
                    int projectId = Integer.parseInt(scanner.nextLine());

                    String response = taskService.removeTasksByProjectId(projectId);
                    printResponse(response);

                    break;
                }
                case "task-create": {
                    System.out.println("Введите id проекта:");
                    int projectId = Integer.parseInt(scanner.nextLine());

                    System.out.println("Введите название задачи:");
                    String taskName = scanner.nextLine().trim();

                    String response = taskService.createTask(taskName, projectId);

                    printResponse(response);
                    break;
                }
                case "task-list": {
                    System.out.println("Введите id проекта:");
                    int projectId = Integer.parseInt(scanner.nextLine());

                    Map<Integer, Task> tasks = taskService.getTasksByProjectId(projectId);

                    tasks.forEach((integer, task) -> System.out.println(task));
                    break;
                }
                case "task-remove": {
                    System.out.println("Введите id задачи:");
                    int id = Integer.parseInt(scanner.nextLine());

                    String response = taskService.removeTaskById(id);

                    printResponse(response);
                    break;
                }
                case "pg": {
                    System.out.println("Введите кол-во проектов для генерации:");
                    int count = Integer.parseInt(scanner.nextLine());

                    for (int i = 1; i <= count; i++) {
                        String name = "Проект #" + i;
                        projectService.createProjectByName(name);
                    }

                    printResponse("[ПРОЕКТЫ СГЕНЕРИРОВАНЫ]");
                    break;
                }
                case "tg": {
                    Map<Integer, Project> projects = projectService.getAllProjects();

                    for (Map.Entry<Integer, Project> projectEntry: projects.entrySet()) {
                        int count = 1 + (int)(Math.random() * 8);

                        for (int i = 1; i <= count; i++) {
                            String name = "Задача #" + i;
                            taskService.createTask(name, projectEntry.getKey());
                        }
                    }

                    printResponse("[ЗАДАЧИ СГЕНЕРИРОВАНЫ]");
                    break;
                }
                case "exit": {
                    System.out.println("*** GOODBYE ***");
                    break;
                }
                default: {
                    System.out.println("Неизвестная команда");
                    break;
                }
            }
        } while (!command.equals("exit"));

    }

    public static void showCommandList() {
        System.out.println("help: Show all commands.");
        System.out.println("project-clear: Remove all projects.");
        System.out.println("project-create: Create new project.");
        System.out.println("project-list: Show all projects.");
        System.out.println("project-remove: Remove selected project.");
        System.out.println("task-clear: Remove all tasks.");
        System.out.println("task-create: Create new task.");
        System.out.println("task-list: Show all tasks.");
        System.out.println("task-remove: Remove selected tasks.");
        System.out.println("pg: Project generator.");
        System.out.println("tg: Task generator.");
    }

    public static void printResponse(String response) {
        System.out.printf("%s \n Введите команду: \n", response);
    }
}
